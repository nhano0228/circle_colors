#include <pebble.h>
Window *base_watchface;
TextLayer *time_text_layer, *battery_text_layer, *am_pm_layer;
Layer *circle_layer, *bg_lines;
static char time_char[] = "00:00", am_pm_char[] = "AM";
static char battery_char[] = "100 Percent";
static GFont jaapokki_font_20, jaapokki_font_17, time_font;
BitmapLayer *bottom_bg, *background;
static PropertyAnimation *change_circle_animation, *remove_circle;
GBitmap *background_img;

void fake_process (Layer *layer, GContext *ctx) {}

void circle_process (Layer *layer, GContext *ctx) {
  int random_number = rand();
  random_number = random_number % 23;
  GColor changing_color;

  switch (random_number) {
    case 0:
      changing_color = GColorRed;
      break;
    case 1:
      changing_color = GColorBlue;
      break;
    case 2:
      changing_color = GColorDarkGreen;
      break;
    case 3:
      changing_color = GColorRajah;
      break;
    case 4:
      changing_color = GColorLimerick;
      break;
    case 5:
      changing_color = GColorWindsorTan;
      break;
    case 6:
      changing_color = GColorBulgarianRose;
      break;
    case 7:
      changing_color = GColorBrilliantRose;
      break;
    case 8:
      changing_color = GColorSunsetOrange;
      break;
    case 9:
      changing_color = GColorLavenderIndigo;
      break;
    case 10:
      changing_color = GColorTiffanyBlue;
      break;
    case 11:
      changing_color = GColorCeleste;
      break;
    case 12:
      changing_color = GColorMayGreen;
      break;
    case 13:
      changing_color = GColorIcterine;
      break;
    case 14:
      changing_color = GColorMediumSpringGreen;
      break;
    case 15:
      changing_color = GColorArmyGreen;
      break;
    case 16:
      changing_color = GColorRoseVale;
      break;
    case 17:
      changing_color = GColorBrightGreen;
      break;
    case 18:
      changing_color = GColorInchworm;
      break;
    case 19:
      changing_color = GColorDarkCandyAppleRed;
      break;
    case 20:
      changing_color = GColorOxfordBlue;
      break;
    case 21:
      changing_color = GColorCadetBlue;
      break;
    case 22:
      changing_color = GColorPastelYellow;
      break;
    case 23:
      changing_color = GColorChromeYellow;
      break;
  }
  graphics_context_set_fill_color(ctx, GColorWhite);
  graphics_fill_circle(ctx, GPoint(72, 68), 68);
 
  graphics_context_set_fill_color(ctx, changing_color);
  graphics_fill_circle(ctx, GPoint(72, 68), 65);
}

void tick_handler_main (struct tm *t, TimeUnits tu) {
  strftime(time_char, sizeof(time_char), "%I\n%M", t);
  strftime(am_pm_char, sizeof(am_pm_char), "%p", t);
  text_layer_set_text(time_text_layer, time_char);
  text_layer_set_text(am_pm_layer, am_pm_char);

  GRect original_place = GRect(0, 0, 144, 168);
  GRect second_place = GRect(144, 0, 144, 168);
  GRect third_place = GRect(-144, 0, 144, 168);

  remove_circle = property_animation_create_layer_frame(circle_layer, &original_place, &second_place);
  animation_set_duration((Animation*)remove_circle, 1500);
  animation_set_delay((Animation*)remove_circle, 750);
  animation_schedule((Animation*) remove_circle);

  change_circle_animation = property_animation_create_layer_frame(circle_layer, &third_place, &original_place);
  animation_set_duration((Animation*)change_circle_animation, 1500);
  animation_set_delay((Animation*)change_circle_animation, 1500);
  animation_schedule((Animation*) change_circle_animation);
}

void battery_handler(BatteryChargeState BCS) {
  int charged_percent = BCS.charge_percent;
  snprintf(battery_char, sizeof(battery_char), "%d Percent", charged_percent);
  text_layer_set_text(battery_text_layer, battery_char);

  if (charged_percent > 50) {
    bitmap_layer_set_background_color(bottom_bg, GColorJaegerGreen);
  } else if (charged_percent > 20) {
    bitmap_layer_set_background_color(bottom_bg, GColorYellow);
  } else {
    bitmap_layer_set_background_color(bottom_bg, GColorFolly);
  }
}

void watchface_load (Window *window) {
  window_set_background_color(window, GColorBlack);
  Layer *window_layer = window_get_root_layer(window);

  background_img = gbitmap_create_with_resource(RESOURCE_ID_IMAGE_BACKGROUND);

  background = bitmap_layer_create(GRect(0,0,144,168));
  bitmap_layer_set_bitmap(background, background_img);
  layer_add_child(window_layer, bitmap_layer_get_layer(background));

  jaapokki_font_20 = fonts_load_custom_font(resource_get_handle(RESOURCE_ID_JAAPOKKI_FONT_20));
  jaapokki_font_17 = fonts_load_custom_font(resource_get_handle(RESOURCE_ID_JAAPOKKI_FONT_17));
  time_font = fonts_load_custom_font(resource_get_handle(RESOURCE_ID_NUMBER_FONT_55));

  circle_layer = layer_create(GRect(0, 0, 144, 168));
  layer_set_update_proc(circle_layer, circle_process);
  layer_add_child(window_layer, circle_layer);

  bottom_bg = bitmap_layer_create(GRect(0, 140, 144, 58));
  bitmap_layer_set_background_color(bottom_bg, GColorWhite);
  layer_add_child(window_layer, bitmap_layer_get_layer(bottom_bg));

  time_text_layer = text_layer_create(GRect(0, 5, 144, 168));
  text_layer_set_text_alignment(time_text_layer, GTextAlignmentCenter);
  text_layer_set_background_color(time_text_layer, GColorClear);
  text_layer_set_text_color(time_text_layer, GColorWhite);
  text_layer_set_font(time_text_layer, time_font);
  layer_add_child(circle_layer, text_layer_get_layer(time_text_layer));

  am_pm_layer = text_layer_create(GRect(114, 143, 144, 168));
  //text_layer_set_text_alignment(am_pm_layer, GTextAlignmentCenter);
  text_layer_set_background_color(am_pm_layer, GColorClear);
  text_layer_set_text_color(am_pm_layer, GColorWhite);
  text_layer_set_font(am_pm_layer, jaapokki_font_17);
  layer_add_child(window_layer, text_layer_get_layer(am_pm_layer));
  
  battery_text_layer = text_layer_create(GRect(3, 143, 100, 30));
  text_layer_set_background_color(battery_text_layer, GColorClear);
  //text_layer_set_text_alignment(battery_text_layer, GTextAlignmentCenter);
  text_layer_set_text_color(battery_text_layer, GColorWhite);
  text_layer_set_font(battery_text_layer, jaapokki_font_17);
  layer_add_child(window_layer, text_layer_get_layer(battery_text_layer));

  BatteryChargeState bcs = battery_state_service_peek();
  battery_handler(bcs);
}

void watchface_unload (Window *window) {
  text_layer_destroy(time_text_layer);
  text_layer_destroy(battery_text_layer);
  bitmap_layer_destroy(bottom_bg);
  bitmap_layer_destroy(background);
  fonts_unload_custom_font(jaapokki_font_17);
  fonts_unload_custom_font(jaapokki_font_20);
  fonts_unload_custom_font(time_font);
  animation_destroy((Animation*)change_circle_animation);
  animation_destroy((Animation*)remove_circle);
}


void init() {
  base_watchface = window_create();
  
  srand(time(NULL));

  window_set_window_handlers(base_watchface, (WindowHandlers) {
    .load = watchface_load,
    .unload = watchface_unload
  });
  
  tick_timer_service_subscribe(MINUTE_UNIT, tick_handler_main);
  battery_state_service_subscribe(battery_handler);
  
  window_stack_push(base_watchface, true);
}
  
void deinit() {
  tick_timer_service_unsubscribe();
  battery_state_service_unsubscribe();
}
  
int main() {
  init();
  app_event_loop();
  deinit();
}